/*=========================================================================================
		File Name: datatables-select.js
		Description: Select Datatable
		----------------------------------------------------------------------------------------
		Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
	 Version: 3.0
		Author: PIXINVENT
		Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function() {

	var tbl_vieworganization = $('#tbl-vieworganization').DataTable({
		columnDefs: [{
			orderable: false,
			className: 'select-checkbox',
			targets:   0
		}],
		select: {
			style:    'multi',
			selector: 'td:first-child'
		},
		order: [[ 1, 'asc' ]],
	});

	tbl_vieworganization.columns().every(function () {
		var that = this;

		$('input', this.footer()).on('keyup change', function () {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		})

	});

	$('#tbl-vieworganization').find('tbody td:last-child').each(function () {
		if (!$(this).hasClass('text-center')) {
			$(this).addClass('text-center')
		}
		$(this).html("<a href='#delete-data'><i class='ficon ft-trash-2'></i></a>");
	});

	$('#tbl-vieworganization-select-all').on('click', function () {
		tbl_vieworganization.rows({page: 'current'}).select();
	});
	$('#tbl-vieworganization-clear-all').on('click', function () {
		tbl_vieworganization.rows().deselect();
	});

	var tbl_organization = $('#tbl-organization').DataTable({
		columnDefs: [{
			orderable: false,
			className: 'select-checkbox',
			targets:   0
		}],
		select: {
			style:    'multi',
			selector: 'td:first-child'
		},
		order: [[ 1, 'asc' ]],
	});

	tbl_organization.columns().every(function () {
		var that = this;

		$('input', this.footer()).on('keyup change', function () {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		})

	});

	$('#tbl-organization-select-all').on('click', function () {
		tbl_organization.rows({page: 'current'}).select();
	});
	$('#tbl-organization-clear-all').on('click', function () {
		tbl_organization.rows().deselect();
	});

	var tbl_course = $('#tbl-course').DataTable({
		columnDefs: [{
			orderable: false,
			className: 'select-checkbox',
			targets:   0
		}],
		select: {
			style:    'multi',
			selector: 'td:first-child'
		},
		order: [[ 1, 'asc' ]],
	});

	tbl_course.columns().every(function () {
		var that = this;
		
		$('input', this.footer()).on('keyup change', function () {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		});

	});

	$('#tbl-course-select-all').on('click', function () {
		tbl_course.rows({page: 'current'}).select();
	});
	$('#tbl-course-clear-all').on('click', function () {
		tbl_course.rows().deselect();
	});

	// $('#save-data').on('click', function () {
	// 	var organizations = tbl_organization.rows('.selected').data().toArray();
	// 	console.log(organizations);
	// 	// var rowData = tableEvents.rows( indexes ).data().toArray();
	// 	// events.prepend( '<div><b>'+type+' selection</b> - '+JSON.stringify( rowData )+'</div>' );
	// })

	$('#save-data').on('click',function(){
		toastr.success('Messages goes here!', 'Title');
	});

	$('.switchBootstrap').bootstrapSwitch();

	$('#switchType').on('switchChange.bootstrapSwitch', function (event, state) {
		alert(state);
	})

});