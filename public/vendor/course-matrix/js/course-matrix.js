$(document).ready(function() {

	// BEGIN: JS for view tab
	$('.switchBootstrap').bootstrapSwitch();

	var tbl_view_org = $('#tbl-vieworganization'),
		switchType = $('#switchType'),
		view_routes = tbl_view_org.attr('data-route');
	
	var view_cols = [
		{ "data": "id" },
		{ "data": "orgCode" },
		{ "data": "orgName" },
		{ "data": "fullname" },
		{ "data": "delete_url" },
	];

	switchViewTable(view_routes, view_cols);
	
	if ($.fn.dataTable.isDataTable('#tbl-vieworganization')) {
		$('#tbl-vieworganization').DataTable().ajax.reload();
	}

	switchType.on('switchChange.bootstrapSwitch', function (event, state) {
		if(state) {
			view_routes = $(this).attr('data-on-routes');
			tbl_view_org.find('thead tr').html(
				"<th></th>" +
				"<th>Organization Code</th>" +
				"<th>Organization Name</th>" +
				"<th>Course Name</th>" +
				"<th class='text-center'>Action</th>"
			);

			view_cols = [
				{ "data": "id" },
				{ "data": "orgCode" },
				{ "data": "orgName" },
				{ "data": "fullname" },
				{ "data": "delete_url" },
			];
		}
		else {
			view_routes = $(this).attr('data-off-routes');
			tbl_view_org.find('thead tr').html(
				"<th></th>" +
				"<th>Job Title Code</th>" +
				"<th>Job Title Name</th>" +
				"<th>Course Name</th>" +
				"<th class='text-center'>Action</th>"
			);

			view_cols = [
				{ "data": "id" },
				{ "data": "jobTitleCode" },
				{ "data": "jobTitleName" },
				{ "data": "fullname" },
				{ "data": "delete_url" },
			];
		}
		switchViewTable(view_routes, view_cols);
	});

	tbl_view_org.on('click', '.delete-matrix', function (e) {
		e.preventDefault();
		var url = $(this).attr('href');
		swal({
		    title: "Are you sure?",
			text: "All selected Course Matrix will be delete and cannot be recovered after you delete it!",
		    icon: "error",
		    buttons: {
                cancel: {
                    text: "No, cancel plx!",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: false,
                },
                confirm: {
                    text: "Yes, delete it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
		    }
		})
		.then((isConfirm) => {
		    if (isConfirm) {
				$.ajax({
					type: 'post',
					url: url,
					data: {'_method': 'delete'},
					dataType: 'json',
					success: function (response) {
						if (response.statusCode == 200) {
							swal("Deleted!", response.message, "success");
							tbl_view_org.DataTable().ajax.reload();
						}
						else {
							swal("Error!", response.message, "error");
						}
					}
				});
		    }
		});
	});

	$('#tbl-vieworganization-select-all').on('click', function () {
		tbl_view_org.DataTable().rows({page: 'current'}).select();
	});
	$('#tbl-vieworganization-clear-all').on('click', function () {
		tbl_view_org.DataTable().rows().deselect();
	});
	$('#tbl-vieworganization-delete-selected').on('click', function () {
		var type = switchType.bootstrapSwitch('state') ? 'organization' : 'jobtitle';
		var data = tbl_view_org.DataTable().rows({selected: true}).data().toArray();
		var url = $(this).attr('data-routes');
		if (data.length == 0) {
			return toastr.warning('Please Select at Least One Data', 'No Data Selected');
		}

		swal({
		    title: "Are you sure?",
			text: "Matrix will be delete and cannot be recovered after you delete it!",
		    icon: "error",
		    buttons: {
                cancel: {
                    text: "No, cancel plx!",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: false,
                },
                confirm: {
                    text: "Yes, delete it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
		    }
		})
		.then((isConfirm) => {
		    if (isConfirm) {
				$.ajax({
					type: 'post',
					url: url,
					data: {'_method': 'delete', data: data, type: type},
					dataType: 'json',
					success: function (response) {
						if (response.statusCode == 200) {
							swal("Deleted!", response.message, "success");
							tbl_view_org.DataTable().ajax.reload();
						}
						else {
							swal("Error!", response.message, "error");
						}
					}
				});
		    }
		});

	});

	// END: JS for view tab

	// BEGIN: JS for create tab

	var organization_routes = $('#tbl-organization').attr('data-route'),
			tbl_organization = $('#tbl-organization'),
			tbl_course = $('#tbl-course'),
			switchTypeCreate = $('#switchTypeCreate');

	var columns = [
		{ "data": "orgId" },
		{ "data": "orgCode" },
		{ "data": "orgName" },
	];

	switchTypeCreate.on('switchChange.bootstrapSwitch', function (event, state) {
		if (state) {
			organization_routes = $(this).attr('data-on-routes');
			tbl_organization.find('thead').html(
				"<tr>" +
				"<th></th>" +
				"<th>Organization Code</th>" +
				"<th>Organization Name</th>" +
				"</tr>"
			);
			columns = [
				{ "data": "orgId" },
				{ "data": "orgCode" },
				{ "data": "orgName" },
			];
		}
		else {
			organization_routes = $(this).attr('data-off-routes');
			tbl_organization.find('thead').html(
				"<tr>" +
				"<th></th>" +
				"<th>Job Title Code</th>" +
				"<th>Job Title Name</th>" +
				"</tr>"
			);
			columns = [
				{ "data": "jobTitleId" },
				{ "data": "jobTitleCode" },
				{ "data": "jobTitleName" },
			];
		}

		switchCreateTable(organization_routes, columns);
	});

	switchCreateTable(organization_routes, columns);

	$('#tbl-organization-select-all').on('click', function () {
		tbl_organization.DataTable().rows({page: 'current'}).select();
	});
	$('#tbl-organization-clear-all').on('click', function () {
		tbl_organization.DataTable().rows().deselect();
	});

	var course_routes = tbl_course.attr('data-route');
	var dt_course = tbl_course.DataTable({
		processing: true,
		serverSide: true,
		deferRender: true,
		ajax: {
			url: course_routes,
			dataType: 'json',
		},
		columns: [
			{ "data": "id" },
			{ "data": "shortname" },
			{ "data": "fullname" },
		],
		columnDefs: [
			{
				orderable: false,
				className: 'select-checkbox',
				targets:   0
			},
			{
				targets: 0,
				render: function (data, type, row) {
					return '';
				}
			}
		],
		select: {
			style:    'multi',
			selector: 'td:first-child'
		},
		order: [[ 1, 'asc' ]],
	});

	dt_course.columns().every(function () {
		var that = this;
		
		$('input', this.footer()).on('keyup change', function () {
			if (that.search() !== this.value) {
				console.log(that.search(this.value));
				that.search(this.value).draw();
			}
		});

	});

	$('#tbl-course-select-all').on('click', function () {
		dt_course.rows({page: 'current'}).select();
	});
	$('#tbl-course-clear-all').on('click', function () {
		dt_course.rows().deselect();
	});

	$('#save-data').on('click',function(){
		var url = $(this).attr('data-route');
		var data;
		if (switchTypeCreate.bootstrapSwitch('state')) {
			var organizations = tbl_organization.DataTable().rows({selected: true}).data().toArray();
			var courses = dt_course.rows({selected: true}).data().toArray();
			if (organizations.length == 0) {
				return toastr.warning('Please Select at least one Organizations', 'No Organization Selected');
			}
			
			if (courses.length == 0) {
				return toastr.warning('Please Select at least one Course', 'No Course Selected');
			}

			data = {
				'organizations': organizations,
				'courses': courses,
				'type': 'organization',
			};
		}
		else {
			var jobtitles = tbl_organization.DataTable().rows({selected: true}).data().toArray();
			var courses = dt_course.rows({selected: true}).data().toArray();
			if (jobtitles.length == 0) {
				return toastr.warning('Please Select at least one Job Titles', 'No Job Title Selected');
			}
			
			if (courses.length == 0) {
				return toastr.warning('Please Select at least one Course', 'No Course Selected');
			}
			data = {
				'jobtitles': jobtitles,
				'courses': dt_course.rows({selected: true}).data().toArray(),
				'type': 'jobtitle',
			};
		}

		$.ajax({
			type: 'post',
			url: url,
			data: data,
			dataType: 'json',
			success: function (response) {
				var notifications = $('#alert-session');
				if (response.statusText == 'success') {
					toastr.success(response.message, 'Course Selected');
					notifications.html("<h5><strong class='text-success'>Course Matrix successfully created</strong></h5>").fadeIn("slow", function () {
						$(this).delay(5000).fadeOut("slow");
					})
				}
				else {
					toastr.error(response.message, 'Error!');
				}
			}
		});
	});
	// END: JS for create tab

});

// View Tab Datatable Switcher
function switchViewTable(url, columns)
{
	if ($.fn.dataTable.isDataTable('#tbl-vieworganization')) {
		$('#tbl-vieworganization').DataTable().destroy();
	}

	var dt_view = $('#tbl-vieworganization').DataTable({
		processing: true,
		serverSide: true,
		deferRender: true,
		ajax: {
			url: url,
			dataType: 'json',
		},
		columns: columns,
		columnDefs: [
			{
				orderable: false,
				className: 'select-checkbox',
				targets:   0
			},
			{
				targets: 0,
				render: function (data, type, row) {
					return '';
				}
			},
			{
				targets: 4,
				className: 'text-center',
			}
		],
		select: {
			style:    'multi',
			selector: 'td:first-child'
		},
		order: [[ 1, 'asc' ]],
	});

	dt_view.columns().every(function () {
		var that = this;

		$('input', this.footer()).on('keyup change', function () {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		})

	});

}

// Create Tab Datatable Switcher
function switchCreateTable(url, columns)
{
	if ($.fn.dataTable.isDataTable('#tbl-organization')) {
		$('#tbl-organization').DataTable().destroy();
	}
	var dt_organization = $('#tbl-organization').DataTable({
		processing: true,
		serverSide: true,
		deferRender: true,
		ajax: {
			url: url,
			dataType: 'json',
		},
		columns: columns,
		columnDefs: [
			{
				orderable: false,
				className: 'select-checkbox',
				targets:   0
			},
			{
				targets: 0,
				render: function (data, type, row) {
					return '';
				}
			}
		],
		select: {
			style:    'multi',
			selector: 'td:first-child'
		},
		order: [[ 1, 'asc' ]],
	});

	dt_organization.columns().every(function () {
		var that = this;

		$('input', this.footer()).on('keyup change', function () {
			if (that.search() !== this.value) {
				that.search(this.value).draw();
			}
		})

	});

}