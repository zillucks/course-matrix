<!-- Checkbox selection -->
<section id="checkbox">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Organizations</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<fieldset>
                            <div class="float-left">
                                <input
                                type="checkbox"
                                class="switchBootstrap"
                                id="switchType"
                                data-on-text="Organization"
                                data-off-text="JobTitle"
                                data-label-text="Type"
                                data-off-color="warning"
                                data-on-routes="{{ route('course::data.company-course', 'organization') }}"
                                data-off-routes="{{ route('course::data.company-course', 'jobtitle') }}"
                                checked />
                            </div>
                        </fieldset>
					</div>
                </div>
                <div class="card-content">
                    <div class="card-body card-dashboard dataTables_wrapper">
                        <table data-route="{{ route('course::data.company-course', 'organization') }}" class="table table-sm table-striped table-bordered" id="tbl-vieworganization" style="width: 100%">
                            <thead>
                                <tr>
                                    <th style="width: 10%"></th>
                                    <th style="width: 20%">Organization Code</th>
                                    <th style="width: 30%">Organization Name</th>
                                    <th style="width: 30%">Course Name</th>
                                    <th style="width: 10%">Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th><input type="text" placeholder="Search By Code" /></th>
                                    <th><input type="text" placeholder="Search Organization" /></th>
                                    <th><input type="text" placeholder="Search Course" /></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        <div class="btn-group btn-group-sm">
                            <button type="button" class="btn btn-primary" id="tbl-vieworganization-select-all">Select All</button>
                            <button type="button" class="btn btn-success" id="tbl-vieworganization-clear-all">Clear All</button>
                            <button data-routes="{{ route('course::matrix.delete.selected') }}" type="button" class="btn btn-danger" id="tbl-vieworganization-delete-selected">Delete Selected</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Checkbox selection -->