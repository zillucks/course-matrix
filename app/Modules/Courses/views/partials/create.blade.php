<!-- Checkbox selection -->
<section id="checkbox">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Organizations</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <fieldset>
                                <div class="float-left">
                                    <input
                                        type="checkbox"
                                        class="switchBootstrap"
                                        id="switchTypeCreate"
                                        data-on-text="Organization"
                                        data-off-text="JobTitle"
                                        data-label-text="Type"
                                        data-off-color="warning"
                                        data-on-routes="{{ route('course::data.organization') }}"
                                        data-off-routes="{{ route('course::data.job-title') }}"
                                        checked />
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard dataTables_wrapper" id="create-tab-table">
                            <table data-route="{{ route('course::data.organization') }}" class="table table-sm table-striped table-bordered" id="tbl-organization" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th style="width: 5%!important"></th>
                                        <th style="width: 25%!important">Organization Code</th>
                                        <th style="width: 70%!important">Organization Name</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th><input type="text" placeholder="Search By Code" /></th>
                                        <th><input type="text" placeholder="Search By Name" /></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-primary" id="tbl-organization-select-all">Select All</button>
                                <button type="button" class="btn btn-success" id="tbl-organization-clear-all">Clear All</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Courses</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard dataTables_wrapper">
                            <table data-route="{{ route('course::data.course') }}" class="table table-sm table-striped table-bordered" id="tbl-course" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th style="width: 5%"></th>
                                        <th style="width: 40%">Course Code</th>
                                        <th style="width: 55%">Course Name</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th><input type="text" placeholder="Search Code" /></th>
                                        <th><input type="text" placeholder="Search Name" /></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-primary" id="tbl-course-select-all">Select All</button>
                                <button type="button" class="btn btn-success" id="tbl-course-clear-all">Clear All</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body text-center">
                        <button data-route="{{ route('course::save') }}" class="btn btn-outline-primary" id="save-data">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Checkbox selection -->