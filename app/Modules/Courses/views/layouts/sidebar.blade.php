<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item">
                <a href="{{ route('course::view') }}">
                    <i class="la la-th-large"></i>
                    <span data-i18n="nav.datatable_extensions.dt_extensions_autofill">Course Matrix</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->