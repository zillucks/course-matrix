<!-- BEGIN: Vendor JS-->
<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/vendors.min.js') }}"></script>
<!-- END: Vendor JS-->

<!-- BEGIN: Datatables Plugin JS -->
<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js') }}"></script>
<!-- END: Datatables Plugin JS -->

<!-- BEGIN: Toastr Plugin JS -->
<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
<!-- END: Toastr Plugin JS -->

<!-- Sweetalert Plugin JS -->
<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/extensions/sweetalert.min.js') }}"></script>

<!-- BEGIN: Theme JS-->
<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/js/core/app-menu.js') }}"></script>
<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/js/core/app.js') }}"></script>
<!-- END: Theme JS-->