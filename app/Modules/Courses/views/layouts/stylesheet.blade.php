<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/css/vendors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/css/extensions/toastr.css') }}">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/css/bootstrap-extended.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/css/colors.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/css/components.css') }}">
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/css/core/menu/menu-types/vertical-menu-modern.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/css/core/colors/palette-gradient.css') }}">

<!-- Toastr Stylesheet -->
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/css/plugins/extensions/toastr.css') }}">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/assets/css/style.css') }}">
<!-- END: Custom CSS-->