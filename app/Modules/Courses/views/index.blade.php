@extends('course::layouts.app')

@push('styles')
	
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/css/forms/toggle/switchery.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/css/plugins/forms/switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/fonts/simple-line-icons/style.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/course-matrix/modern-admin/app-assets/css/core/colors/palette-switch.css') }}">
	
@endpush

@section('content')
	<div class="content-wrapper">
		<div class="content-header row mb-1">
			<div class="content-header-left col-md-6 col-12 mb-2">
				<h3 class="content-header-title mb-0 d-inline-block">Course Matrix</h3>
			</div>
		</div>
		<div class="content-body">
			<section id="tabs">
				<ul class="nav nav-tabs nav-underline no-hover-bg">
					<li class="nav-item">
						<a class="nav-link active" id="view-tab" data-toggle="tab" aria-controls="viewtab" href="#viewtab" aria-expanded="false">View</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="create-tab" data-toggle="tab" aria-controls="createtab" href="#createtab" aria-expanded="true">Create</a>
					</li>
				</ul>
				<div class="tab-content px-1 pt-1">
					<div role="tabpanel" class="tab-pane fade show active" id="viewtab" aria-expanded="true" aria-labelledby="view-tab">
						@include('course::partials.view')
					</div>
					<div class="tab-pane fade" id="createtab" aria-expanded="true" aria-labelledby="create-tab">
						@include('course::partials.create')
					</div>
				</div>
			</section>
		</div>
	</div>
@endsection

@push('scripts')
    <script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/forms/toggle/switchery.min.js') }}"></script>
	<script src="{{ asset('vendor/course-matrix/modern-admin/app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js') }}"></script>
	
	<!-- BEGIN: Page JS-->
	<script src="{{ asset('vendor/course-matrix/js/course-matrix.js') }}"></script>
	<!-- END: Page JS-->
@endpush