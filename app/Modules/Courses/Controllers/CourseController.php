<?php

namespace App\Modules\Courses\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Courses\Models\Course;
use App\Modules\Courses\Models\Organization;
use App\Modules\Courses\Models\CourseMatrix;
use App\Modules\Courses\Response;

use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{

    private $course;
    private $organization;

    public function __construct()
    {
        $this->course = new Course;
        $this->organization = new Organization;
    }

    public function index(Request $request)
    {
        return view('course::index');
    }

    public function create(Request $request)
    {
        return view('course::create');
    }

    public function createTab(Request $request, $tabs)
    {
        return view('course::partials.' . $tabs);
    }

    public function save(Request $request)
    {
        $response = new Response();

        if (!$request->isMethod('post')) {
            $response->statusCode = 405;
            $response->message = 'Method Not Allowed';
            return $response->fail();
        }

        $courses = $request->courses;

        DB::beginTransaction();
        try {
            if ($request->get('type') == 'organization') {
                $organizations = $request->organizations;
                foreach($organizations as $i => $organization) {
                    foreach($courses as $j => $course) {
                        $checkExists = CourseMatrix::where([
                            ['orgCode', '=', $organization['orgCode']],
                            ['courseId', '=', $course['id']],
                        ])->count();
                        
                        if ($checkExists > 0) {
                            DB::rollback();
                            $response->statusCode = 500;
                            $response->statusText = 'error';
                            $response->message = 'Organization ' . $organization['orgCode'] . ' already picked ' . $course['fullname'] . ' Course';
                            return $response->fail();
                        }

                        $matrix = new CourseMatrix();
                        $matrix->orgCode = $organization['orgCode'];
                        $matrix->type = $request->get('type');
                        $matrix->courseId = $course['id'];
                        $matrix->save();
                    }
                }
                $message = 'Saving Courses by Organizations Success';
            }
            if ($request->get('type') == 'jobtitle') {
                $jobtitles = $request->jobtitles;
                foreach($jobtitles as $i => $jobtitle) {
                    foreach($courses as $j => $course) {
                        $checkExists = CourseMatrix::where([
                            ['jobTitleCode', '=', $jobtitle['jobTitleCode']],
                            ['courseId', '=', $course['id']],
                        ])->count();
                        
                        if ($checkExists > 0) {
                            DB::rollback();
                            $response->statusCode = 500;
                            $response->statusText = 'error';
                            $response->message = 'Job Title ' . $jobtitle['jobTitleName'] . ' already picked ' . $course['fullname'] . ' Course';
                            return $response->fail();
                        }

                        $matrix = new CourseMatrix();
                        $matrix->jobTitleCode = $jobtitle['jobTitleCode'];
                        $matrix->type = $request->get('type');
                        $matrix->courseId = $course['id'];
                        $matrix->save();
                    }
                }
                $message = 'Saving Courses by JobTitles Success';
            }

            DB::commit();
            $response->statusCode = 200;
            $response->statusText = 'success';
            $response->message = $message;
            return $response->success();
        }
        catch(\Exception $e) {
            DB::rollback();
            $response->statusCode = $e->getCode();
            $response->statusText = 'error';
            $response->message = $e->getMessage();
            return $response->fail();
        }

    }

    public function deleteMatrix($id, $type)
    {
        $matrix = CourseMatrix::where([
            ['id', '=', $id],
            ['type', '=', $type]
        ])->first();

        $response = new Response();
        try{
            $matrix->delete();
            $response->statusCode = 200;
            $response->statusText = 'deleted';
            $response->message = 'Course Matrix Successfully Deleted';
            return $response->success();
        }
        catch(\Exception $e) {
            $response->statusCode = $e->getCode;
            $response->statusText = 'error';
            $response->message = $e->getMessage();
            return $response->fail();
        }

        return $matrix;
    }

    public function bulkDeleteMatrix(Request $request)
    {
        $type = $request->get('type');
        $data = $request->get('data');

        $response = new Response();
        DB::beginTransaction();
        try {
            foreach($data as $i => $value) {
                $matrix = CourseMatrix::find($value['id']);
                $matrix->delete();
            }
            DB::commit();
            $response->statusCode = 200;
            $response->statusText = 'deleted';
            $response->message = 'All selected Course Matrix Successfully Deleted';
            return $response->success();
        }
        catch (\Exception $e) {
            DB::rollback();
            $response->statusCode = $e->getCode;
            $response->statusText = 'error';
            $response->message = $e->getMessage();
            return $response->fail();
        }
    }
}
