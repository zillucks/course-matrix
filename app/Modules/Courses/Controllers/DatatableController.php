<?php

namespace App\Modules\Courses\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Courses\Models\Course;
use App\Modules\Courses\Models\CourseMatrix;
use App\Modules\Courses\Models\JobTitle;
use App\Modules\Courses\Models\Organization;
use Auth;

class DatatableController extends Controller
{
    private $course;
    private $organization;
    private $jobTitle;
    public function __construct()
    {
        $this->course = new Course;
        $this->organization = new Organization;
        $this->jobTitle = new JobTitle;
    }

    public function organization(Request $request)
    {
        $data = $this->organization->all();
        $columns = [
            0 => 'orgId',
            1 => 'orgCode',
            2 => 'orgName',
        ];

        $totalData = $this->organization->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $organizations = $this->organization->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value');

            $organizations = $this->organization->where('orgCode','LIKE',"%{$search}%")
                        ->orWhere('orgName', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = $this->organization->where('orgCode','LIKE',"%{$search}%")
                        ->orWhere('orgName', 'LIKE',"%{$search}%")
                        ->count();
        }

        $data = [];
        if (!empty($organizations)) {
            foreach ($organizations as $organization) {
                $cols['orgId'] = $organization->orgId;
                $cols['orgCode'] = $organization->orgCode;
                $cols['orgName'] = $organization->orgName;

                $data[] = $cols;
            }
        }

        $json = [
            'draw'              => intval($request->input('draw')),
            'recordsTotal'      => intval($totalData),
            'recordsFiltered'   => intval($totalFiltered),
            'data'              => $data
        ];

        return response()->json($json);
    }

    public function jobTitle(Request $request)
    {
        $data = $this->jobTitle->all();
        $columns = [
            0 => 'jobTitleId',
            1 => 'jobTitleCode',
            2 => 'jobTitleName',
        ];

        $totalData = $this->jobTitle->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start') ?? 1;
        $order = $columns[$request->input('order.0.column') ?? 1];
        $dir = $request->input('order.0.dir') ?? 'asc';

        if(empty($request->input('search.value')))
        {
            $jobTitles = $this->jobTitle
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
        }
        else {
            $search = $request->input('search.value');

            $jobTitles = $this->jobTitle->where('jobTitleCode','LIKE',"%{$search}%")
                    ->orWhere('jobTitleName', 'LIKE',"%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

            $totalFiltered = $this->jobTitle->where('jobTitleCode','LIKE',"%{$search}%")
                    ->orWhere('jobTitleName', 'LIKE',"%{$search}%")
                    ->count();
        }

        $data = [];
        if (!empty($jobTitles)) {
            foreach ($jobTitles as $jobTitle) {
                $cols['jobTitleId'] = $jobTitle->jobTitleId;
                $cols['jobTitleCode'] = $jobTitle->jobTitleCode;
                $cols['jobTitleName'] = $jobTitle->jobTitleName;

                $data[] = $cols;
            }
        }

        $json = [
            'draw'              => intval($request->input('draw')),
            'recordsTotal'      => intval($totalData),
            'recordsFiltered'   => intval($totalFiltered),
            'data'              => $data
        ];

        return response()->json($json);
    }

    public function course(Request $request)
    {
        $request->request->add(['username' => Auth::user()->username ?? 'johndoe']);
        $structure_course = $this->course->structure_course($request);
        
        $getData = $structure_course->getData();
        if(is_null($getData->data)) {
            $data = collect([]);
        }
        else {
            $data = collect($getData->data->allCourseList);
        }

        $columns = [
            0 => 'id',
            1 => 'shortname',
            2 => 'fullname',
        ];

        $totalData = $data->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir') == 'asc' ? 0 : 1;

        if(empty($request->input('search.value')))
        {
            $courses = $data->forPage($start, $limit)
                    ->sortBy($order,$dir)
                    ->all();
        }
        else {
            $search = $request->input('search.value');
            $courses = $data
                    ->filter(function ($value, $key) use ($search) {
                        return false !== (stristr($value->shortname, $search) || stristr($value->fullname, $search));
                    })
                    ->forPage($start, $limit)
                    ->sortBy($order,$dir)
                    ->all();

            $totalFiltered = $data
                ->filter(function ($value, $key) use ($search) {
                    return false !== (stristr($value->shortname, $search) || stristr($value->fullname, $search));
                })
                ->count();
        }

        $data = [];
        if (!empty($courses)) {
            foreach ($courses as $course) {
                $cols['id'] = $course->id;
                $cols['shortname'] = $course->shortname;
                $cols['fullname'] = $course->fullname;

                $data[] = $cols;
            }
        }

        $json = [
            'draw'              => intval($request->input('draw')),
            'recordsTotal'      => intval($totalData),
            'recordsFiltered'   => intval($totalFiltered),
            'data'              => $data
        ];

        return response()->json($json);
    }

    public function companyCourse(Request $request, $type)
    {
        $matrix = new CourseMatrix();
        $request->request->add([
            'username' => 'johndoe',
            'type' => $type
        ]);

        $data = $matrix->company_course($request);

        if ($type == 'organization') {
            $columns = [
                0   => 'id',
                1   => 'orgCode',
                2   => 'orgName',
                3   => 'fullname',
                4   => 'delete_url',
            ];
        }
        elseif ($type == 'jobtitle') {
            $columns = [
                0   => 'id',
                1   => 'jobTitleCode',
                2   => 'jobTitleName',
                3   => 'fullname',
                4   => 'delete_url',
            ];
        }

        $totalData = $data->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start') ?? 1;
        $order = $columns[$request->input('order.0.column') ?? 1];
        $dir = $request->input('order.0.dir') == 'asc' ? 0 : 1;

        if(empty($request->input('search.value')))
        {
            $courses = $data
                    ->forPage($start, $limit)
                    ->sortBy($order,$dir)
                    ->all();
        }
        else {
            $search = $request->input('search.value');

            $courses = $data
                    ->filter(function ($value, $key) use ($search) {
                        return false !== (stristr($value->orgCode, $search) || stristr($value->orgName, $search) || stristr($value->fullname, $search));
                    })
                    ->forPage($start, $limit)
                    ->sortBy($order,$dir)
                    ->all();

            $totalFiltered = $data
                    ->filter(function ($value, $key) use ($search) {
                        return false !== (stristr($value->orgCode, $search) || stristr($value->orgName, $search) || stristr($value->fullname, $search));
                    })
                    ->forPage($start, $limit)
                    ->count();

        }

        $data = [];
        if (!empty($courses)) {
            if ($type == 'organization') {
                foreach ($courses as $course) {
                    $cols['id'] = $course->id;
                    $cols['orgCode'] = $course->orgCode;
                    $cols['orgName'] = $course->orgName;
                    $cols['fullname'] = $course->fullname;
                    $cols['delete_url'] = "<a class='text-danger justify-content-center delete-matrix' href='". route('course::matrix.delete', ['id' => $course->id, 'type' => $course->type]) ."'><i class='ficon ft-trash-2'></i></a>";
    
                    $data[] = $cols;
                }
            }
            elseif ($type == 'jobtitle') {
                foreach ($courses as $course) {
                    $cols['id'] = $course->id;
                    $cols['jobTitleCode'] = $course->jobTitleCode;
                    $cols['jobTitleName'] = $course->jobTitleName;
                    $cols['fullname'] = $course->fullname;
                    $cols['delete_url'] = "<a class='text-danger justify-content-center delete-matrix' href='". route('course::matrix.delete', ['id' => $course->id, 'type' => $course->type]) ."'><i class='ficon ft-trash-2'></i></a>";
    
                    $data[] = $cols;
                }
            }
        }

        $json = [
            'draw'              => intval($request->input('draw')),
            'recordsTotal'      => intval($totalData),
            'recordsFiltered'   => intval($totalFiltered),
            'data'              => $data
        ];

        return response()->json($json);
    }
}
