<?php

namespace App\Modules\Courses\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use View;

class CourseServiceProvider extends ServiceProvider
{

    protected $namespace = 'App\Modules\Courses';

    public function boot()
    {
        parent::boot();

        $this->publishes([
            __DIR__ . '/../assets' => public_path('vendor/course-matrix')
        ], 'public');
    }

    public function register()
    {
        $this->app->make('App\Modules\Courses\Controllers\CourseController');
        // View::addNamespace('course', app_path('Modules/Courses/views'));
        $this->loadViewsFrom(__DIR__ . '/../views', 'course');
    }

    public function map()
    {
        $this->mapWebRoutes();
    }

    public function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace . '\Controllers')
            ->group(app_path('Modules/Courses/routes/web.php'));
    }
}
