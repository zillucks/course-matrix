<?php

// Route::get('/', function () {
//     return view('datatable');
// });

Route::prefix('course')->group(function () {
    Route::name('course::')->group(function () {
        Route::get('/', 'CourseController@index')->name('view');
        Route::post('save', 'CourseController@save')->name('save');
        Route::delete('matrix/{id}/{type}/delete', 'CourseController@deleteMatrix')->name('matrix.delete');
        Route::delete('matrix/delete', 'CourseController@bulkDeleteMatrix')->name('matrix.delete.selected');

        // Route::get('create/tab/{tab}', 'CourseController@createTab')->name('create.tab');

        Route::prefix('data')->group(function () {
            Route::name('data.')->group(function () {
                Route::get('course', 'DatatableController@course')->name('course');
                Route::get('organization', 'DatatableController@organization')->name('organization');
                Route::get('job-title', 'DatatableController@jobTitle')->name('job-title');
                Route::get('company-course/{type}', 'DatatableController@companyCourse')->name('company-course');
            });
        });
    });
});