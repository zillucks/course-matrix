<?php

namespace App\Modules\Courses;

use Illuminate\Http\Request;

class Response
{
    public $statusCode;
    public $statusText;
    public $message;
    public $data;

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getStatusText()
    {
        return $this->statusText;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    public function setStatusText($statusText)
    {
        $this->statusText = $statusText;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function fail()
    {
        $this->statusCode = 200;
        $this->statusText = 'fail';
        return response()->json($this->response());
    }

    public function error()
    {
        $this->statusCode = 500;
        $this->statusText = 'error';
        return response()->json($this->response());
    }

    public function done()
    {
        $this->statusCode = 200;
        $this->statusText = 'done';
        return response()->json($this->response());
    }

    public function success()
    {
        $this->statusCode = 200;
        $this->statusText = 'success';
        return response()->json($this->response());
    }

    private function response()
    {
        return [
            'statusCode'    => $this->statusCode,
            'statusText'    => $this->statusText,
            'message'       => $this->message,
            'data'          => $this->data
        ];
    }
}
