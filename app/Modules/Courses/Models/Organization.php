<?php

namespace App\Modules\Courses\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Organization extends Model
{
    protected $table = 'colibri_organization';
    protected $primaryKey = 'orgId';
    public $timestamps = false;
}