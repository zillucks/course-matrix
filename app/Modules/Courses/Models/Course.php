<?php

namespace App\Modules\Courses\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Modules\Courses\Response as Res;

class Course extends Model
{
	protected $table = 'mdl_course';
	public $timestamps = false;

	public function structure_course(Request $req)
	{
		$res = new Res();
		
		$allCategoryList = DB::table('mdl_course_categories')
			->select('mdl_course_categories.id', 'mdl_course_categories.name', 'mdl_course_categories.idnumber', 'mdl_course_categories.parent')
			->where('id', '!=', 1)
			->get();
		$emp = DB::table('colibri_employee')
			->select('compCode')
			->where('empCode', '=', $req->username)
			->get();
			
		if(count($emp) == 0)
		{
			$res->message = 'empCode : `'.$req->username.'` in colibri_employee not found';
			return $res->fail();
			// return response()->json([
			// 	'statusCode' => 204,
			// 	'statusText' => 'no-content',
			// 	'message' => 'empCode : `'.$req->username.'` in colibri_employee not found'
			// ]);
		}
		$courseid = DB::table('colibri_course_addon')
			->select('courseId')
			->where('compCode', '=', $emp[0]->compCode)
			->get();

		$array_course = array();    
		if(count($courseid) == 0)
		{
			$res->message = 'company tidak memiliki course';
			return $res->fail();
		}else{          
			if(count($courseid)){
				foreach ($courseid as $key => $value) {
					$array_course[] = $value->courseId;
				}
			}
		}
		
		$allCourseList = DB::table(DB::raw('mdl_course c'))
			->select('c.id', 'c.fullname', 'c.category', 'c.shortname', 'c.idnumber', 'c.summary', 'c.startdate', 'c.enddate', DB::raw('sub.name as subCategoryName'), DB::raw('cat.name as categoryName'), DB::raw('prog.name as programName'))
			->leftJoin(DB::raw('mdl_course_categories sub'), 'sub.id', '=', 'c.category')
			->leftJoin(DB::raw('mdl_course_categories cat'), 'cat.id', '=', 'sub.parent')
			->leftJoin(DB::raw('mdl_course_categories prog'), 'prog.id', '=', 'cat.parent')
			->whereIn('c.id', $array_course)
			->where('format', 'topics')
			->orderBy('c.sortorder', 'ASC')
			->get();

		$allBatchList = DB::table('colibri_course_batch')->get();

		$allInclassPrereqList = DB::table('colibri_inclass_prereq')->get();

		$allInclassList = DB::table('colibri_inclass')->get();
		$programList    = [];
		foreach ($allCategoryList as &$program) {
			if ($program->parent == 0) {
				$categoryList = [];
				foreach ($allCategoryList as &$category) {
					if ($category->parent == $program->id) {
						$subCategoryList = [];
						foreach ($allCategoryList as &$subCategory) {
							if ($subCategory->parent == $category->id) {
								$courseList = [];
								foreach ($allCourseList as &$course) {
									if ($course->category == $subCategory->id) {
										$batchList = [];
										foreach ($allBatchList as $batch) {
											if ($batch->courseId = $course->id) {
												$inclassPrereqList = [];
												foreach ($allInclassPrereqList as $inclassPrereq) {
													if ($inclassPrereq->batchId == $batch->batchId) {
														$inclassPrereqList[] = $inclassPrereq;
													}
												}
												$batch->inclassPrereqList = $inclassPrereqList;
												$inclassList              = [];
												foreach ($allInclassList as $inclass) {
													if ($inclass->batchId == $batch->batchId) {
														$inclassList = $inclass;
													}
												}
												$batch->inclassList = $inclassList;
												$batchList[]        = $batch;
											}
										}
										$moodle_url = urlencode("http://colibri.towert.win/core/course/view.php&id=".$course->id);
										$moodle_url = "http://colibri.towert.win/core/me/service.php?class=auth&mehod=autologin&url=".$moodle_url."&id=2";
										$course->moodle_url = $moodle_url;
										$course->edit       = false;
										$course->batchList  = $batchList;
										$courseList[]       = $course;
									}
								}
								$subCategory->edit       = false;
								$subCategory->courseList = $courseList;
								$subCategoryList[]       = $subCategory;
							}
						}
						$category->edit            = false;
						$category->subCategoryList = $subCategoryList;
						$categoryList[]            = $category;
					}
				}
				$program->edit         = false;
				$program->categoryList = $categoryList;
				$programList[]         = $program;
			}
		}

		$res->data = [
			'programList'   => $programList,
			'allCourseList' => $allCourseList,
		];
		return $res->done();
		// return view('training-catalogue')->with($data);
	}
}
