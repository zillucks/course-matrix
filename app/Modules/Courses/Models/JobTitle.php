<?php

namespace App\Modules\Courses\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class JobTitle extends Model
{
    protected $table = 'colibri_job_title';
    protected $primaryKey = 'jobTitleId';
    public $timestamps = false;
}