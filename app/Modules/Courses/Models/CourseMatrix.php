<?php

namespace App\Modules\Courses\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Modules\Courses\Response;

class CourseMatrix extends Model
{
    protected $table = 'colibri_matrix';
    public $timestamps = false;

    public function company_course(Request $request)
    {
        $response = new Response;
        $type = $request->has('type') ? $request->get('type') : 'organization';
        
        if(empty($type)) {
            $response->message = 'Please Choose Type';
            return $response->fail();
        }

        if ($type == 'organization') {
            $data = $this->join('colibri_organization', 'colibri_matrix.orgCode', '=', 'colibri_organization.orgCode')
                ->join('colibri_employee', function ($join) use ($request) {
                    $join->on('colibri_organization.orgCode', '=', 'colibri_employee.orgCode')
                        ->where('colibri_employee.empCode', '=', $request->get('username'));
                })
                ->join('mdl_course', 'colibri_matrix.courseId', '=', 'mdl_course.id')
                ->select('colibri_matrix.id', 'colibri_matrix.orgCode', 'colibri_matrix.type', 'colibri_organization.orgName', 'colibri_matrix.courseId', 'mdl_course.shortname', 'mdl_course.fullname')
                ->get();
        }
        else {
            $data = $this->join('colibri_job_title', 'colibri_matrix.jobTitleCode', '=', 'colibri_job_title.jobTitleCode')
                ->join('colibri_employee', function ($join) use ($request) {
                    $join->on('colibri_matrix.jobTitleCode', '=', 'colibri_job_title.jobTitleCode')
                        ->where('colibri_employee.empCode', '=', $request->get('username'));
                })
                ->join('mdl_course', 'colibri_matrix.courseId', '=', 'mdl_course.id')
                ->select('colibri_matrix.id', 'colibri_matrix.jobTitleCode', 'colibri_matrix.type', 'colibri_job_title.jobTitleName', 'colibri_matrix.courseId', 'mdl_course.shortname', 'mdl_course.fullname')
                ->get();
        }

        return $data;
    }
}